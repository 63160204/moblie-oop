import "dart:io";

class Doll {
  String? name;
  String? color;
  int age = 0;

  Doll(String name, String color, int age) {
    this.name = name;
    this.color = color;
    this.age = age;
  }
  void speak() {
    print(("name: " +
        this.name.toString() +
        "\n" +
        "color: " +
        this.color.toString() +
        "\n" +
        "age: " +
        this.age.toString() +
        "year"));
  }

  void walk() {}
}
