import 'Doll.dart';
import 'MonkeyDoll.dart';
import 'TeddyBear.dart';
import 'CatDoll.dart';

void main(List<String> args) {
  MonkeyDoll D1 =
      new MonkeyDoll("MIMI", "White ", 1);
  D1.speak();
  D1.walk();
 
  TeddyBear D2 = new TeddyBear("MUMU ","Red ", 2);
  D2.speak();
  D2.walk();

  CatDoll D3 = new CatDoll("MeowMeow ", "Black ",3);
  D3.speak();
  D3.walk();
}
  
